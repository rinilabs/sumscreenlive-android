package com.rishness.sumscreenlive;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Brian Roy Villanueva on 20/02/2016.
 */

public class AppSettings {

	private static AppSettings mAppSettings;
	private Context mContext;
	public static final String PREF_SETTINGS = "SETTINGS";
	private static final String PREF_BACKGROUND = "background";
	private static final String PREF_LASTCHANNEL = "lastchannel";
	public String lastchannel;
	public String background;

	public AppSettings(Context context){
		SharedPreferences pref = context.getSharedPreferences(PREF_SETTINGS,Context.MODE_PRIVATE);
		load(pref);
		mContext = context;
	}

	void load(SharedPreferences pref) {
		// defaults
		background  = pref.getString(PREF_BACKGROUND, "#ffa500");
		lastchannel = pref.getString(PREF_LASTCHANNEL, "pewdiepie");
		Log.i("AppSettings","lastChannel->"+lastchannel); // Info
	}

	private void save() {

		SharedPreferences pref = mContext.getSharedPreferences(PREF_SETTINGS, Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = pref.edit();
		edit.putString(PREF_BACKGROUND, background);
		edit.putString(PREF_LASTCHANNEL, lastchannel);

		edit.commit();

	}

	//SET BG COLOR
	public void setBgColor(String bgcolor) {background = bgcolor; save(); }

	public String getBgColor() {return background; }

	//SET channel
	public void setChannel(String channel) {
		lastchannel = channel;
		save();
	}

	public String getChannel() {
		return lastchannel;
	}


	public static  AppSettings getInstance(Context context){
		if(mAppSettings == null){
			mAppSettings = new AppSettings(context);
		}
		return mAppSettings;
	}
}
