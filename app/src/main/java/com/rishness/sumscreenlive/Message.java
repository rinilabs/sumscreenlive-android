package com.rishness.sumscreenlive;

/**
 * Created by Brian Roy Villanueva on 20/02/2016.
 */
public class Message {

    private String channelID;
    private String title;
    private String subCount;
    private String imgUrl;

    public Message() {
        super();
    }

    public Message(String channelID, String title, String subCount, String imgUrl) {
        super();
        this.channelID = channelID;
        this.title = title;
        this.subCount = subCount;
        this.imgUrl = imgUrl;
    }
    public String getChannelID() {
        return channelID;
    }

    public void setChannelID(String id) {
        this.channelID = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubCount() {
        return subCount;
    }

    public void setSubCount(String subCount) {
        this.subCount = subCount;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
/*
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + channelID;
        return result;
    }*/

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Message other = (Message) obj;
        if (channelID != other.channelID)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Product [channelID=" + channelID + ", title=" + title + ", subCount="
                + subCount + ", imgUrl=" + imgUrl + "]";
    }
}
