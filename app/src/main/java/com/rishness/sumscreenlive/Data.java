package com.rishness.sumscreenlive;

/**
 * Created by Brian Roy Villanueva on 20/02/2016.
 */

public class Data {
	public String channelID;
	public String subCount;
	public String title;
	public String imgUrl;
	public String hasImage;
	public String color;
	public String channelName;
	public int isBookmark;

}
