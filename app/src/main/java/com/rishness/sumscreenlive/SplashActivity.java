package com.rishness.sumscreenlive;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Brian Roy Villanueva on 20/02/2016.
 */

public class SplashActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            // TODO Auto-generated method stub
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.content_splash);

            Thread timerThread = new Thread(){
                public void run(){
                    try{
                        sleep(3000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }finally{

                        Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                        startActivity(intent);

                         //Intent intent = new Intent(SplashActivity.this,GuestActivity.class);
                         //startActivity(intent);

                         //Intent intent = new Intent(SplashActivity.this,MemberActivity.class);
                         //startActivity(intent);

                        // Intent intent = new Intent(SplashActivity.this,ProspectorActivity.class);
                        //  startActivity(intent);
                    }
                }
            };
            timerThread.start();
        }

        @Override
        protected void onPause() {
            // TODO Auto-generated method stub
            super.onPause();
            finish();
        }
}
