package com.rishness.sumscreenlive;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.rishness.sumscreenlive.app.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Brian Roy Villanueva on 20/02/2016.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TOAST_TEXT = "Test ads are being shown. "
            + "To show live ads, replace the ad unit ID in res/values/strings.xml with your own ad unit ID.";

    String LOG = "MainActivity";

    // URL to get contacts JSON
    private static String urlString="";
    // JSON Node names
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_CHANNEL_ID = "channelId";
    private static final String TAG_SUB_COUNT = "subCount";
    private static final String TAG_TITLE = "title";
    private static final String TAG_IMG_URL = "imgUrl";
    private static final String TAG_HAS_IMAGE = "hasImage";
    private static final String TAG_COLOR= "color";
    private static final String TAG_CHANNEL_NAME = "channelName";

    String channelID;
    String subCount;
    String title;
    String imgUrl;
    String hasImage;
    String color;
    String channelName;

    // contacts JSONArray
    JSONArray contacts = null;

    // Hashmap for ListView
    ArrayList<HashMap<String, String>> contactList;

    Button btnAdd,btnDel,btnShare,btnSettings;
    ImageView profile;
    RelativeLayout view;
    TextView tvCount;

    AppSettings mAppSettings;

    String currentSelection="";
    Firebase myFirebaseRef;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options;

    private TextSwitcher mSwitcher;

    View rootView;

    SharedPreference sharedPreference;

    String lastChannel;

    SharedPreferences settings;

    ArrayList<Message> products;

    SQLiteAdapter mySQLiteAdapter;

    String isBookmark;

    String jsonStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        setContentView(R.layout.content_main);
        createDB();

        MyApplication.getInstance().trackScreenView("MainScreen");

        rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        mSwitcher = (TextSwitcher) findViewById(R.id.textSwitcher);
        // Set the ViewFactory of the TextSwitcher that will create TextView object when asked
        mSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                // TODO Auto-generated method stub
                // create new textView and set the properties like clolr, size etc
                TextView myText = new TextView(MainActivity.this);
                myText.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                myText.setTextSize(70);
                myText.setTextColor(Color.BLACK);
                myText.setTypeface(Typeface.DEFAULT_BOLD);
               // myText.setShadowLayer(10, 10, 10, Color.BLACK);
                return myText;
            }
        });

        // Declare the in and out animations and initialize them
        Animation in = AnimationUtils.loadAnimation(this,android.R.anim.fade_in);
        Animation out = AnimationUtils.loadAnimation(this,android.R.anim.fade_out);

        // set the animation type of textSwitcher
        mSwitcher.setInAnimation(in);
        mSwitcher.setOutAnimation(out);

        Firebase.setAndroidContext(this);

        options = new DisplayImageOptions.Builder()
                //.showImageOnLoading(R.drawable.ic_stub)
                //.showImageForEmptyUri(R.drawable.no_image)
                //.showImageOnFail(R.drawable.no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(this)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);

        initView(); // Initialized View

        mAppSettings = AppSettings.getInstance(this);

        //lastChannel = mAppSettings.getChannel();

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            currentSelection = (String) bd.get("currentChannel");

        } else {

            //currentSelection = "pewdiepie";
            currentSelection = mAppSettings.getChannel();

        }

        Log.i(LOG,"lastChannel->"+lastChannel); // Info

        /*
        if(lastChannel != null || lastChannel.equals("")){

            currentSelection = mAppSettings.getChannel();

        }else{

            currentSelection = "pewdiepie";
        }

        */
        /**
        if (currentSelection.equals("") || currentSelection.equals(null)){

            currentSelection = "pewdiepie";

        } else {

            currentSelection = mAppSettings.getChannel();

        }*/

        Log.i(LOG,"currentSelection->"+currentSelection); // Info

        urlString="http://live.sumscreen.com/ytuser/"+currentSelection;

        Log.i(LOG,"-"+urlString); // Info

        // Calling async task to get json
        new getData().execute();

        // Load an ad into the AdMob banner view.
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);

        // Toasts the test ad message on the screen. Remove this after defining your own ad unit ID.
        //Toast.makeText(this, TOAST_TEXT, Toast.LENGTH_LONG).show();
    }

    public void initView(){

        btnAdd = (Button)findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        btnDel = (Button)findViewById(R.id.btnDel);
        btnDel.setOnClickListener(this);
        btnShare = (Button)findViewById(R.id.btnShare);
        btnShare.setOnClickListener(this);
        btnSettings = (Button)findViewById(R.id.btnSettings);
        btnSettings.setOnClickListener(this);
        profile = (ImageView) findViewById(R.id.profile);
        view = (RelativeLayout) findViewById(R.id.RL1);
        //tvCount= (TextView) findViewById(R.id.tvCount);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case  R.id.btnAdd: {
                //btnDel.setVisibility(View.VISIBLE);
                ViewDialogAdd alert = new ViewDialogAdd();
                alert.showDialog(this, "Add to Bookmarks?");

                break;
            }
            case R.id.btnDel: {
                //btnDel.setVisibility(View.GONE);
                ViewDialogDelete alert = new ViewDialogDelete();
                alert.showDialog(this, "Delete from Bookmarks?");


                break;
            }

            case R.id.btnShare: {
                takeScreenshot();
                break;
            }

            case R.id.btnSettings: {
                // OPEN SETTINGS
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                finish();
                break;
            }
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class getData extends AsyncTask<Void, Void, Void> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            jsonStr = sh.makeServiceCall(urlString, ServiceHandler.GET);

            Log.d("Response:", "-> " + jsonStr);

            if (jsonStr != null || jsonStr.contains("error") || jsonStr.equals("")) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    contacts = jsonObj.getJSONArray(TAG_MESSAGE);

                    // looping through All Contacts
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);

                        channelID = c.getString(TAG_CHANNEL_ID);
                        subCount = c.getString(TAG_SUB_COUNT);
                        title = c.getString(TAG_TITLE);
                        imgUrl = c.getString(TAG_IMG_URL);
                        hasImage = c.getString(TAG_HAS_IMAGE);
                        color = c.getString(TAG_COLOR);
                        channelName = c.getString(TAG_CHANNEL_NAME);

                        //mAppSettings.setBgColor(color);
                        mAppSettings.setChannel(channelID);

                        Log.v(LOG,"channelID->"+channelID); // Verbose
                        Log.d(LOG,"subCount->"+subCount); // Debug
                        Log.i(LOG,"title->"+title); // Info
                        Log.w(LOG,"imgUrl->"+imgUrl); // Warning
                        Log.e(LOG,"hasImage->"+hasImage); // Error
                        Log.w(LOG,"color->"+color); // Warning
                        Log.e(LOG,"channelName->"+channelName); // Error

                        myFirebaseRef = new Firebase("https://livecount.firebaseio.com/channels/"+channelID);

                        Log.v(LOG,"myFirebaseRef =" + myFirebaseRef); // Verbose

                        myFirebaseRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                Log.v(LOG,"snapshot.getValue() ="+ snapshot.getValue()); // Verbose
                                Log.v(LOG,"snapshot.getChildrenCount() ="+ snapshot.getChildrenCount()); // Verbose
                                //long qActive = (long) snapshot.child("subCount").getValue();
                                //Log.v(LOG,"qActive ="+ qActive); // Verbose
                                mSwitcher.setText(new DecimalFormat("###,###,###.##").format(snapshot.child("subCount").getValue()));

                            }
                            @Override
                            public void onCancelled(FirebaseError firebaseError) {
                                //System.out.println("The read failed: " + firebaseError.getMessage());
                                Log.v(LOG,"-"+ firebaseError.getMessage()); // Verbose
                            }
                        });

                        /**
                        // tmp hashmap for single contact
                        HashMap<String, String> contact = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        contact.put(TAG_CHANNEL_ID, channelID);
                        contact.put(TAG_SUB_COUNT, subCount);
                        contact.put(TAG_TITLE, title);
                        contact.put(TAG_IMG_URL, imgUrl);
                        contact.put(TAG_HAS_IMAGE, hasImage);
                        contact.put(TAG_COLOR, color);
                        contact.put(TAG_CHANNEL_NAME, channelName);

                        // adding contact to contact list
                        contactList.add(contact);
                        */


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            if (jsonStr.equals("error")){

                ViewDialog alert = new ViewDialog();
                alert.showDialog(MainActivity.this, "Not found", "We could not find the channel");

            }

            imageLoader.displayImage(imgUrl, profile);

            String currentColor=mAppSettings.getBgColor();

            Log.v(LOG,"currentColor->" + mAppSettings.getBgColor()); // Verbose

            if (currentColor != null || currentColor.equals("")){

                view.setBackgroundColor(Color.parseColor(mAppSettings.getBgColor()));

            }else {

                view.setBackgroundColor(Color.parseColor(color));

            }

            mySQLiteAdapter = new SQLiteAdapter(MainActivity.this);
            mySQLiteAdapter.openToRead();

            isBookmark=mySQLiteAdapter.getValue("SELECT isBookmark FROM livecount WHERE channelID='"+channelID+"' AND isBookmark='1' ");

            mySQLiteAdapter.close();

            Log.v(LOG,"isBookmark->"+ isBookmark); // Verbose

            if (isBookmark.equals("1")){
                btnDel.setVisibility(View.VISIBLE);
            } else {
                btnDel.setVisibility(View.GONE);
            }

            /**
            mySQLiteAdapter = new SQLiteAdapter(MainActivity.this);
            mySQLiteAdapter.openToRead();

            channelID=mySQLiteAdapter.getValue("SELECT channelID FROM livecount  WHERE channelID='"+channelID+"'");

            mySQLiteAdapter.close();

            Log.v(LOG,"channelID-"+ channelID); // Verbose

            if (channelID != null){

                btnAdd.setVisibility(View.GONE);
                btnDel.setVisibility(View.VISIBLE);

            } else {

                btnAdd.setVisibility(View.VISIBLE);
                btnDel.setVisibility(View.GONE);
            }
            */


            /**
            myFirebaseRef = new Firebase("https://livecount.firebaseio.com/channels/"+channelID);

            Log.v(LOG,"myFirebaseRef =" + myFirebaseRef); // Verbose

            myFirebaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    Log.v(LOG,"snapshot.getValue() ="+ snapshot.getValue()); // Verbose
                    Log.v(LOG,"snapshot.getChildrenCount() ="+ snapshot.getChildrenCount()); // Verbose
                    long qActive = (long) snapshot.child("subCount").getValue();
                    Log.v(LOG,"qActive ="+ qActive); // Verbose
                    mSwitcher.setText(new DecimalFormat("###,###,###.##").format(qActive));


                }
                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    //System.out.println("The read failed: " + firebaseError.getMessage());
                    Log.v(LOG,"-"+ firebaseError.getMessage()); // Verbose
                }
            });
            */
                /**
                 * Updating parsed JSON data into ListView
                 * */
             /*
            ListAdapter adapter = new SimpleAdapter(
                    MainActivity.this, contactList,
                    R.layout.list_item, new String[] { TAG_NAME, TAG_EMAIL,
                    TAG_PHONE_MOBILE }, new int[] { R.id.name,
                    R.id.email, R.id.mobile });

            setListAdapter(adapter);
            */

        }
    }

    /**
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */

    @Override
    public void onResume(){
        super.onResume();

    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //openScreenshot(imageFile);
            shareMe(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }
    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    private void shareMe(File imageFile){

        String message="UPDATE: "+title+"current subscriber count is "+subCount+". Download #LiveCount Android app - via @sumscreen";
        Uri imageUri = Uri.fromFile(imageFile);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.setType("image/jpeg");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "send"));

    }

    public void createDB(){

        mySQLiteAdapter = new SQLiteAdapter(this);
        mySQLiteAdapter.openToRead();

        String query="CREATE TABLE IF NOT EXISTS livecount (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "channelID TEXT,subCount TEXT,title TEXT,imgUrl TEXT,isBookmark INTEGER);";

        mySQLiteAdapter.execQuery(query);
        mySQLiteAdapter.close();

        Log.v("query","query= "+query);
    }

    public class ViewDialogAdd {

        public void showDialog(Activity activity, String msg){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_background);

            TextView text = (TextView) dialog.findViewById(R.id.txtMsg);
            text.setText(msg);

            Button dialogButtonYes = (Button) dialog.findViewById(R.id.diaYes);
            dialogButtonYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    saveBookmark();
                    btnDel.setVisibility(View.VISIBLE);
                }
            });

            Button dialogButtonNo = (Button) dialog.findViewById(R.id.diaNo);
            dialogButtonNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        }
    }

    public class ViewDialogDelete {

        public void showDialog(Activity activity, String msg){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_background);

            TextView text = (TextView) dialog.findViewById(R.id.txtMsg);
            text.setText(msg);

            Button dialogButtonYes = (Button) dialog.findViewById(R.id.diaYes);
            dialogButtonYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    deleteBookmark();
                    btnDel.setVisibility(View.GONE);


                }
            });

            Button dialogButtonNo = (Button) dialog.findViewById(R.id.diaNo);
            dialogButtonNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        }
    }


    public class ViewDialog {

        public void showDialog(Activity activity, String msg, String message){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_background_alert);

            TextView text = (TextView) dialog.findViewById(R.id.txtMsg);
            text.setText(msg);

            TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
            tvMessage.setText(message);

            Button dialogButtonYes = (Button) dialog.findViewById(R.id.diaYes);
            dialogButtonYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    currentSelection = mAppSettings.getChannel();

                    Log.i(LOG,"currentSelection->"+currentSelection); // Info

                    urlString="http://live.sumscreen.com/ytuser/"+currentSelection;

                    Log.i(LOG,"-"+urlString); // Info

                    // Calling async task to get json
                    new getData().execute();
                }
            });

            dialog.show();

        }
    }

    public void saveBookmark(){

        isBookmark="1";
        mySQLiteAdapter = new SQLiteAdapter(MainActivity.this);
        mySQLiteAdapter.openToWrite();
        String updateQuery =
                "INSERT INTO livecount (channelID ,subCount ,title ,imgUrl, isBookmark) VALUES(" +
                        "'" +channelID +
                        "','" + subCount +
                        "','" +title +
                        "','"+ imgUrl +
                        "','"+ isBookmark +
                        "');" +
                        "";

        mySQLiteAdapter.execQuery(updateQuery);
        mySQLiteAdapter.close();
        Log.i(LOG,"updateQuery-"+updateQuery); // Info

    }

    public void deleteBookmark(){

        mySQLiteAdapter = new SQLiteAdapter(this);
        mySQLiteAdapter.openToWrite();
        String sqlQuery="DELETE FROM livecount WHERE channelID='"+channelID+"' AND isBookmark='1'";
        mySQLiteAdapter.execQuery(sqlQuery);
        mySQLiteAdapter.close();
        Log.i(LOG,"sqlQuery->"+sqlQuery); // Info
        //Toast.makeText(getBaseContext(), "Deleted Successfully.", Toast.LENGTH_LONG).show();

    }



}
