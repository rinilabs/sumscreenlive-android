package com.rishness.sumscreenlive;

/**
 * Created by Brian Roy Villanueva on 20/02/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPreference {


    public static final String PREFS_NAME = "PRODUCT_APP";
    public static final String FAVORITES = "Product_Favorite";

    public SharedPreference() {
        super();
    }

    // This four methods are used for maintaining favorites.
    public void saveFavorites(Context context, List<Message> favorites) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        editor = settings.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);
        editor.putString(FAVORITES, jsonFavorites);
        editor.commit();
    }

    public void addFavorite(Context context, Message product) {
        List<Message> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<Message>();
        favorites.add(product);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, Message product) {
        ArrayList<Message> favorites = getFavorites(context);
        if (favorites != null) {
            favorites.remove(product);
            saveFavorites(context, favorites);
        }
    }

    public ArrayList<Message> getFavorites(Context context) {
        SharedPreferences settings;
        List<Message> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Message[] favoriteItems = gson.fromJson(jsonFavorites,
                    Message[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Message>(favorites);
        } else
            return null;

        return (ArrayList<Message>) favorites;
    }
}