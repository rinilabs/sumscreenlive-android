package com.rishness.sumscreenlive;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.rishness.sumscreenlive.app.MyApplication;

import java.util.ArrayList;

/**
 * Created by Brian Roy Villanueva on 19/02/2016.
 */
public class SettingsActivity extends Activity implements View.OnClickListener {

    String TAG = "SettingsActivity";
    Button btnSave;
    Button btn1,btn2,btn3,btn4,btn5;
    AppSettings mAppSettings;
    EditText edChannel;
    SQLiteAdapter mySQLiteAdapter;
    public static ArrayList<Data> dataArray;
    private brxAdapter m_adapter;
    private static ArrayList<Data> m_orders = null;
    private ListView listView;
    private GridView gridView;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.content_settings);
        mAppSettings = AppSettings.getInstance(this);
        initViews();

        MyApplication.getInstance().trackScreenView("SettingScreen");

        options = new DisplayImageOptions.Builder()
                //.showImageOnLoading(R.drawable.ic_stub)
                //.showImageForEmptyUri(R.drawable.no_image)
                //.showImageOnFail(R.drawable.no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(this)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);

        loadData(this);

        m_orders = dataArray;

        m_adapter = new brxAdapter(this, R.layout.item_layout, m_orders);

        //listView = (ListView) findViewById(R.id.contactList);

        //listView.setAdapter(m_adapter);

        gridView = (GridView) findViewById(R.id.gridView);

        gridView.setAdapter(m_adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                TextView channelID = (TextView) v.findViewById(R.id.channelID);
                String currentChannel = channelID.getText().toString();

                //mAppSettings.setChannel(currentChannel);
                Intent settings = new Intent(SettingsActivity.this, MainActivity.class);
                settings.putExtra("currentChannel", currentChannel);
                startActivity(settings);
                finish();
            }
        });

    }

    public void initViews(){

        btn1 = (Button)findViewById(R.id.btn1);
        btn1.setOnClickListener(this);

        btn2 = (Button)findViewById(R.id.btn2);
        btn2.setOnClickListener(this);

        btn3 = (Button)findViewById(R.id.btn3);
        btn3.setOnClickListener(this);

        btn4 = (Button)findViewById(R.id.btn4);
        btn4.setOnClickListener(this);

        btn5 = (Button)findViewById(R.id.btn5);
        btn5.setOnClickListener(this);

        btnSave = (Button)findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        edChannel= (EditText)findViewById(R.id.edChannel);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case  R.id.btn1: {
                mAppSettings.setBgColor("#3F51B5");
                Log.v("SettingsActivity","->" + mAppSettings.getBgColor()); // Verbose
                break;
            }

            case R.id.btn2: {
                mAppSettings.setBgColor("#009688");
                Log.v("SettingsActivity","->" + mAppSettings.getBgColor()); // Verbose
                break;
            }
            case R.id.btn3: {
                mAppSettings.setBgColor("#FFC107");
                Log.v("SettingsActivity","->" + mAppSettings.getBgColor()); // Verbose
                break;
            }
            case R.id.btn4: {
                mAppSettings.setBgColor("#FF0000");
                Log.v("SettingsActivity","->" + mAppSettings.getBgColor()); // Verbose
                break;
            }
            case R.id.btn5: {
                mAppSettings.setBgColor("#F3715B");
                Log.v("SettingsActivity","->" + mAppSettings.getBgColor()); // Verbose
                break;
            }
            case R.id.btnSave: {

                String currentChannel=edChannel.getEditableText().toString();

                if (currentChannel.equals("") || currentChannel.equals(null)){
                    mAppSettings.setChannel(mAppSettings.getChannel());
                    Intent settings = new Intent(SettingsActivity.this, MainActivity.class);
                    //settings.putExtra("currentChannel", currentChannel);
                    startActivity(settings);
                    finish();
                }else{
                    Intent settings = new Intent(SettingsActivity.this, MainActivity.class);
                    settings.putExtra("currentChannel", currentChannel);
                    startActivity(settings);
                    finish();
                }
                break;
            }

        }
    }
    @Override
    public void onBackPressed(){
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }

    public void loadData(Context context){

        mySQLiteAdapter = new SQLiteAdapter(context);
        mySQLiteAdapter.openToRead();

        String sqlQuery;

        dataArray=new ArrayList<Data>();

        sqlQuery="SELECT channelID, subCount,title,imgUrl FROM livecount WHERE isBookmark='1' ORDER BY ID ASC;";

        Cursor temp = mySQLiteAdapter.read(sqlQuery);

        int size = temp.getCount();

        for(int x=0; x<size;x++){
            temp.moveToNext();

            Data itemObj = new Data();

            itemObj.channelID = temp.getString(0);
            itemObj.subCount = temp.getString(1);
            itemObj.title = temp.getString(2);
            itemObj.imgUrl = temp.getString(3);

            dataArray.add(itemObj);

            Log.d(TAG, "itemObj.code"+itemObj.channelID);
            Log.i(TAG, "itemObj.code"+itemObj.subCount);
            Log.v(TAG, "itemObj.name"+itemObj.title);
            Log.d(TAG, "itemObj.latitude"+itemObj.imgUrl);

            itemObj=null;
        }

        mySQLiteAdapter.close();

    }

    private class brxAdapter extends ArrayAdapter<Data> {
        private ArrayList<Data> items;
        private LayoutInflater inflater;
        //private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
        public brxAdapter(Context context, int textViewResourceId, ArrayList<Data> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.item_layout, null);
            }
            Data o = items.get(position);
            if (o != null) {

                TextView channelID = (TextView)v.findViewById(R.id.channelID);
                TextView subCount = (TextView)v.findViewById(R.id.subCount);
                TextView title = (TextView)v.findViewById(R.id.title);
                TextView imgUrl = (TextView)v.findViewById(R.id.imgUrl);
                ImageView img = (ImageView) v.findViewById(R.id.profile);

                if(channelID != null){
                    channelID.setText(o.channelID);}

                if(subCount != null){
                    subCount.setText(o.subCount);}

                if (title != null) {
                    title.setText(o.title);}

                if (imgUrl != null) {
                    imgUrl.setText(o.imgUrl);
                    imageLoader.displayImage(o.imgUrl, img);
                }
                return v;
            }
            return v;
        }
    }
}

